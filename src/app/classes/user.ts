import {MAX_LOGINS} from "../config/login-config";

export class User {
    private _username: string = null;
    private _password: string = null;


    public failedLogins: number = 0;

    constructor(username: string = null, password: string = null) {
        if (username) {
            this._username = username;
        }
        if (password) {
            this._password = password;
        }
    }


    public checkPassword(password: string): boolean {
        if (this._password === password && !this.isBlocked()) {
            this.failedLogins = 0;
            return true;
        } else {
            this.failedLogins++;
            return false;
        }
    }

    public isBlocked(): boolean {
        return this.failedLogins >= MAX_LOGINS;
    }

    get password(): string {
        return this._password;
    }

    get username(): string {
        return this._username;
    }

    set username(value: string) {
        this._username = value;
    }

    set password(value: string) {
        this._password = value;
    }
}
