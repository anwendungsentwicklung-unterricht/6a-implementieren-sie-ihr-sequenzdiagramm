import {Component} from '@angular/core';
import {User} from "./classes/user";
import {LOGIN_INTERFACE, LoginService} from "./services/login.service";

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    public loginUser: User = new User();
    public title: string = 'Login | Timo Köhler';
    public result: LOGIN_INTERFACE = {};

    constructor(public loginService: LoginService) {
        this.loginService.loadTestData();
    }


    public login(): void {
        this.result = this.loginService.login(this.loginUser);
    }

}
