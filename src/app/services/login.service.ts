import {Injectable} from '@angular/core';
import {User} from "../classes/user";
import {TestData} from "../test-data/test-data";
import {MAX_LOGINS} from "../config/login-config";

export interface LOGIN_INTERFACE {
    error?: string;
    success?: boolean;
}

@Injectable({
    providedIn: 'root'
})
export class LoginService {

    public loginData: User[] = [];


    constructor() {
    }

    public loadTestData(): void {
        TestData.forEach(user => {
            this.addLoginData(user);
        });
    }

    private searchUser(username: string): User {
        return this.loginData.find(x => x.username.trim().toLowerCase() === username.trim().toLowerCase());
    }

    public login(user: User): LOGIN_INTERFACE {
        if (user && user.username && user.password) {
            const foundUser: User = this.searchUser(user.username);

            if (!foundUser) {
                return {error: "User nicht gefunden"};
            } else {
                if (foundUser.isBlocked()) {
                    return {error: "Konto Blockiert"};
                } else {
                    if (foundUser.checkPassword(user.password)) {
                        return {success: true};
                    } else {
                        return {error: "Falsche Zugangsdaten!" + (foundUser.isBlocked() ? "\nKonto wurde gesperrt!"
                                : "\n" + foundUser.failedLogins + "/" + MAX_LOGINS + " Versuche!")};
                    }
                }
            }
        } else {
            return {};
        }
    }


    public addLoginData(user: User): void {
        if (this.loginData.filter(x => x.username === user.username).length > 0) {
            this.loginData = this.loginData.filter(x => x.username !== user.username);
        }
        this.loginData.push(user);
    }


}
