import {User} from "../classes/user";

export const TestData: User[] = [
    new User("MaxMustermann", "123456"),
    new User("JohnDoe", "SagIchNicht"),
    new User("JennaDoe", "SagIchAuchNicht"),
];
